var isMobile = window.matchMedia("only screen and (min-width: 551px)");

$(document).ready(function() { 

    $('<img/>').attr('src', $('.hidden').attr('data-src')).load(function() {
       
        $('.hidden').remove(); 
        var imageWidth = this.width;
        var imageHeight = this.height;
        var resizeTimer;
        var resizeDiv = $('.imageResize');
        
        $('.loader').css({
            'left': $(window).width()/2 - 36, 
            'top' : imageHeight/2 - 36 ,
            'opacity' : '1'
        });

        $('body').addClass('readyMainImage');


        setTimeout(addReadyClass, 500);

        if($('.carousel').length > 0) {

            $('.carousel').carousel({
                interval: 4000
            })
            initCarouselIndicators();
        }

        if(resizeDiv.length > 0) {

            setImageHeight(resizeDiv, imageHeight, imageWidth);

            $(window).on('resize', function (e) {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                   setImageHeight(resizeDiv, imageHeight, imageWidth);
                }, 100);
            });
        }

    });
});        


var addReadyClass = function(){
    $('.loader').css('opacity', '0');
    $('.loader').remove();
    $('body').addClass('ready');

};


var setImageHeight = function(resizeDiv, imageHeight, imageWidth) {
    var windowWidth  = $(window).width(),
        $imageHeight = imageHeight,
        $imageWidth = imageWidth,
        $curImage =  resizeDiv,
        aspectRatio  = $imageWidth/$imageHeight;
   
    if (isMobile.matches || windowWidth > 551) {
        carouselHeight = windowWidth / aspectRatio;
        $('a', $curImage).css('min-height', carouselHeight);
    }
    else {
        $('a', $curImage).css('min-height', '');
    }

}

var initCarouselIndicators = function() {
    $(".bottom-nav .table-cell[data-target]").each(function (i, indicators) {
        var targetId = indicators.dataset.target;
        if (targetId != "") {
            var $carousel = $(targetId);
            $carousel.bind('slide.bs.carousel', function (e) {
                var $targetSlide = $(e.relatedTarget);
                var index = $targetSlide.index();
                $('.bottom-nav .table-cell[data-target="' + targetId + '"]').removeClass('active')
                $('.bottom-nav .table-cell[data-target="' + targetId + '"]:nth-child(' + (index) + ')').addClass('active');
            });
        }
    });
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \DB::table('users')->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = User::create(
            [
                'name' => 'Jackspero',
                'email' => 'wikitudedeveloper1@gmail.com',
                'password' => bcrypt(123456),
                'type' => "admin",
            ]
        );

        $user = User::create(
            [
                'name' => 'Chetan Sperow',
                'email' => 'wikitudedeveloper@gmail.com',
                'password' => bcrypt(123456),
                'type' => "admin",
            ]
        );

        


        


    }
}

<?php

return [

    'label'=>[
        'language' => 'Language',
        'email' => 'Email',
        'phone' => 'Phone',
        'enter' => 'Enter',
        'join21bet' => 'Join 21Bet',
        'make_your_picks' => 'Make Your Picks',
        'select_the_outcome_of_every_group_stage_match' => 'Select the outcome of every group stage match',
        'draw' => 'Draw',
        'submit' => 'Submit',
        'enter_email_phone' => 'Please enter you email and phone number !',
        'action_not_procede' => 'Action Not Procede!',
        'home' => 'Home',
        'the_€100k_world_cup_pick_em_terms_and_conditions' => 'THE €100K WORLD CUP PICK-EM - TERMS AND CONDITIONS',
        'qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june' => 'Qualifying will run from 00:01 on Monday 14th May and end at 23:59 on Wednesday 13th June.',
        'to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period' => 'To enter the competition you must have a 21Bet account before the end of the qualifying period.',
        'your_email_address_must_match_your_entry_with_your_21bet_account' => 'Your e-mail address must match your entry with your 21Bet account.',
        'only_1_entry_is_permitted_per_person_per_household' => 'Only 1 entry is permitted per person, per household.',
        'to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page' => 'To qualify for the €100k cash prize you must correctly predict the outcome of all 48 matches in the group stages of the world cup by submitting your picks on the World Cup Pick-em page.',
        'if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally' => 'If more than 1 person predicts correctly the prize pool will be shared equally.',
        'the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded' => 'The cash prize will be paid into your 21Bet account which must be fully KYC, Verified and funded.',
        "customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part" => "Customers who are restricted from promotions will not be eligible to take part. If you are restricted once entered into the promotion, you'll no longer be eligible for any prizes.",
        'players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet' => 'Players must be 18 years of age to enter or to play on 21Bet.',
        'promotions' => 'Promotions',
        'terms_and_conditions' => 'Terms and Conditions',
        'betting_rules' => 'Betting Rules',
        'responsible_gambling' => 'Responsible Gambling',
        'privacy_policy' => 'Privacy Policy',
        'cookies_policy' => 'Cookies Policy',
        'contact_us' => 'Contact Us',
        'copyright' => 'Copyright',
        '2018_21bet_com' => '2018 21bet.com',
        'customers_from_the_UK_are_not_eligible_to_enter' => 'Customers from the UK are not eligible to enter.',
        'review_already_submited' => 'Review Already Submitted !!',
        'review_submited_successfully' => 'Review Submitted Successfully !!',
        'success' => 'Success !!',
   ]

];

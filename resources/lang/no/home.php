<?php

return [

    'label'=>[
        'language' => 'Språk',
        'email' => 'Epost',
        'phone' => 'Telefon',
        'enter' => 'tast inn',
        'join21bet' => 'Bli med på 21bet',
        'make_your_picks' => 'velg ditt valg',
        'select_the_outcome_of_every_group_stage_match' => 'velg utfallet av hver gruppetrinnskamp',
        'draw' => 'Uavgjort',
        'submit' => 'Sende inn',
        'enter_email_phone' => 'Vennligst skriv inn e-post og telefonnummer! Vennligst skriv inn e-post og telefonnummer!',
        'action_not_procede' => 'Handling ikke beskyttet!',
        'home' => 'Hjem',
        'the_€100k_world_cup_pick_em_terms_and_conditions' => '1 Million KR VM Pick-em Konkurranse- Vilkår og betingelser',
        'qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june' => 'Kvalifiseringen gå fra 00:01 mandag 14. mai og slutter klokken 23:59 onsdag 13. juni.',
        'to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period' => 'For å delta i konkurransen det må du ha en 21Bet-konto før slutten av kvalifiseringsperioden',
        'your_email_address_must_match_your_entry_with_your_21bet_account' => 'Din e-postadresse må samsvare med oppføringen din med 21Bet-kontoen din.',
        'only_1_entry_is_permitted_per_person_per_household' => 'Kun 1 oppføring er tillatt per person, per husholdning.',
        'to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page' => 'For å kvalifisere seg til 1 million NOK pengepremien det må du korrekt forutsi utfallet av alle 48 kamper i gruppetrinnene av VM ved å sende inn dine valg på VM Pick-em-siden.',
        'if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally' => 'Hvis mer enn 1 person spår riktig, blir premiepotten delt like.',
        'the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded' => 'Pengepremien blir betalt inn i din 21Bet-konto som må være fullt KYC, verifisert og må saldo.',
        "customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part" => "Kunder som er begrenset fra kampanjer, er ikke kvalifisert til å delta. Hvis du er begrenset når du kommet seg inn i kampanjen, det skal du ikke lenger være kvalifisert for noen premier.",
        'players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet' => 'Spillere må være 18 år for å komme inn eller spille på 21Bet.',
        'promotions' => 'Kampanjer',
        'terms_and_conditions' => 'Regler og vilkår',
        'betting_rules' => 'Spillregler',
        'responsible_gambling' => 'Ansvarlig spill',
        'privacy_policy' => 'Sikkerhet og personvern',
        'cookies_policy' => 'Informasjonskapsler',
        'contact_us' => 'Kontakt oss',
        'copyright' => 'opphavsrett',
        '2018_21bet_com' => '2018 21bet.com',
        'customers_from_the_UK_are_not_eligible_to_enter' => 'Kunder fra Storbritannia kan ikke delta i kampanjen.',
        'review_already_submited' => 'Anmeldelse allerede sendt!',
        'review_submited_successfully' => 'Gjennomgang innsendt vellykket !!',
        'success' => 'Suksess !!',
    ]

];

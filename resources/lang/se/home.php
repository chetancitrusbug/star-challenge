<?php

return [

    'label'=>[
        'language' => 'Språk',
        'email' => 'E-post',
        'phone' => 'Telefon',
        'enter' => 'OK',
        'join21bet' => 'Gå med i 21Bet',
        'make_your_picks' => 'Gör dina val',
        'select_the_outcome_of_every_group_stage_match' => 'Välj resultatet på varje gruppstegsmatch',
        'draw' => 'Oavgjort',
        'submit' => 'Välj',
        'enter_email_phone' => 'Vänligen ange din e-postadress och telefonnummer! Var vänlig skriv in din e-postadress och telefonnummer!',
        'action_not_procede' => 'Åtgärd inte skyddad!',
        'home' => 'Hem',
        'the_€100k_world_cup_pick_em_terms_and_conditions' => '1 miljon kr VM Pick-em tävling – Avtal och regler',
        'qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june' => 'Kvalificerade period är aktiv mellan 00:01 måndag den 14 maj och avslutas 23:59 onsdag den 13 Juni.',
        'to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period' => 'För att gå med i tävlingen måste du ha ett 21Bet-konto innan den kvalificerade perioden avslutas.',
        'your_email_address_must_match_your_entry_with_your_21bet_account' => 'Din e-mailaddress måste vara samma address du använder för ditt 21Bet-konto.',
        'only_1_entry_is_permitted_per_person_per_household' => 'Endast 1 spelare per person och hushåll är tillåten.',
        'to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page' => 'För att kvalificera dig för 1 miljon kr priset måste du korrekt välja resultatet av alla 48 matcher i gruppspelet av VM 2018 genom att välja dessa på vår VM Pick-em sida.',
        'if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally' => 'Om mer än en person väljer korrekt resultat delas prispotten jämnlikt.',
        'the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded' => 'Priset kommer att betalas in till ditt 21Bet-konto vilket måste vara verifierat och innehålla ett saldo.',
        "customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part" => "Spelare som har kampanjrestriktioner på sitt konto kommer ej att kunna ta del av tävlingen. Om ditt konto har restriktioner för kampanjer, detta även om ditt konto blir bonusbegränsat under tävlingens gång.",
        'players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet' => 'Alla spelare måste vara minst 18 år för att spela på 21Bet.',
        'promotions' => 'Kampanjer',
        'terms_and_conditions' => 'Avtal & Villkor',
        'betting_rules' => 'Spelregler',
        'responsible_gambling' => 'Ansvarsfullt spelande',
        'privacy_policy' => 'Säkerhetspolicy',
        'cookies_policy' => 'Cookies Policy',
        'contact_us' => 'Kontakta oss',
        'copyright' => 'upphovsrätt',
        '2018_21bet_com' => '2018 21bet.com',
        'customers_from_the_UK_are_not_eligible_to_enter' => 'Kunder från Storbritannien är kan ej ta del av tävlingen.',
        'review_already_submited' => 'Granska redan inlagt !!',
        'review_submited_successfully' => 'Granska inlämnad framgångsrikt !!',
        'success' => 'Framgång !!',
    ]

];

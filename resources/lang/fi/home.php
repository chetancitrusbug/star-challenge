<?php

return [

    'label'=>[
        'language' => 'Kieli',
        'email' => 'Sähköposti',
        'phone' => 'Puhelinnumero',
        'enter' => 'Syötä',
        'join21bet' => 'Liity 21Bet',
        'make_your_picks' => 'Tee valintasi',
        'select_the_outcome_of_every_group_stage_match' => 'Valitse tulos jokaisessa lohkovaiheen ottelussa',
        'draw' => 'Tasapeli',
        'submit' => 'Lähetä',
        'enter_email_phone' => 'Anna sähköpostiosoitteesi ja puhelinnumerosi!',
        'action_not_procede' => 'Toimi ei mene!',
        'home' => 'Koti',
        'the_€100k_world_cup_pick_em_terms_and_conditions' => '100 000€ Jalkapallon MM-kisat tarjous – Ehdot ja Säännöt',
        'qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june' => 'Osallistumisaika alkaa maanantaina 14. toukokuuta klo 00:01 ja loppuu keskiviikkona 13. kesäkuuta klo 23:59.',
        'to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period' => 'Osallistuaksesi kilpailuun sinun pitää olla liittynyt 21Bet-sivustolle ennen kilpailuajan loppumista.',
        'your_email_address_must_match_your_entry_with_your_21bet_account' => 'Sinun sähköpostiosoitteesi pitää olla sama kuin 21Bet käyttäjätilisi.',
        'only_1_entry_is_permitted_per_person_per_household' => 'Vain yksi osallistuminen sallitaan per henkilö tai per kotitalous.',
        'to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page' => 'Voittaaksesi 100 palkinnon sinun pitää arvata oikein 48 ottelun lopputulos lohkovaiheessa WorldCup Pick-em sivulla.',
        'if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally' => 'Jos enemmän kuin yksi henkilö arvaa lopputulokset oikein, palkinto jaetaan tasan voittajien kesken.',
        'the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded' => 'Palkinto maksetaan 21Bet käyttäjätilille ja sen täytyy olla täysin KYC, vahvistettu ja siellä pitää olla saldoa.',
        "customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part" => "Asiakkaat jotka on bonusrajoitettu, eivät voi ottaa osaa tähän kilpailuun. Jos sinut rajoitetaan kilpailun aikana, et voi enää vastaanottaa palkintoa.",
        'players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet' => 'Osallistujien täytyy olla yli 18-vuotta osallistuakseen kilpailuun tai pelatakseen 21Betin sivustolla.',
        'promotions' => 'Edut & Kampanjat',
        'terms_and_conditions' => 'Ehdot & Säännöt',
        'betting_rules' => 'Vedonlyöntisäännöt',
        'responsible_gambling' => 'Vastuullinen pelaaminen',
        'privacy_policy' => 'Turvallisuus & Yksityisyys',
        'cookies_policy' => 'lisätietoa evästeistä',
        'contact_us' => 'Ota yhteyttä',
        'copyright' => 'tekijänoikeus',
        '2018_21bet_com' => '2018 21bet.com',
        'customers_from_the_UK_are_not_eligible_to_enter' => 'Asiakkaat Iso-Britanniasta eivät ole oikeutettuja osallistumaan.',
        'review_already_submited' => 'Tarkista jo lähetetty !!',
        'review_submited_successfully' => 'Arvostelua lähetettiin onnistuneesti !!',
        'success' => 'Menestys !!',
    ]

];

<?php

return [

    'label'=>[
        'language' => 'Dil',
        'email' => 'Email',
        'phone' => 'Telefon Numarasi',
        'enter' => 'Giris',
        'join21bet' => 'Üyelik aç',
        'make_your_picks' => 'Seçimini yap ',
        'select_the_outcome_of_every_group_stage_match' => 'Her grup aşaması için tahminini gir',
        'draw' => 'Berabere',
        'submit' => 'Gönder',
        'enter_email_phone' => 'Lütfen e-posta ve telefon numaranızı giriniz! Lütfen size e-posta ve telefon numaranızı giriniz!',
        'action_not_procede' => 'Eylem korunmuyor!',
        'home' => 'Ev',
        'the_€100k_world_cup_pick_em_terms_and_conditions' => '100.000 EUR Dünya Kupası Ödülü Kural ve Şartları',
        'qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june' => 'Bu promosyona katılım 14 Mayıs 00:01’de baslayacak ve 13 Haziran Carsamba gunu saat 23:59’da tamamlanacaktir.',
        'to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period' => 'Katilmak icin yukarida belirtilen bitis tarihinden once bir 21Bet hesabiniz olmali.',
        'your_email_address_must_match_your_entry_with_your_21bet_account' => 'Yarismaya katildiginiz adresi ile 21Bet hesabinizdaki adresi ayni olmalidir.',
        'only_1_entry_is_permitted_per_person_per_household' => 'Her kisi ve hane icin sadece tek katilim yapilabilir.',
        'to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page' => '100.000 EUR nakit odulu kazanmak icin Dunya Kupasi Grup asamasindaki 48 macin sonucunu',
        'if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally' => 'Eger 1 kisiden fazla bilen olursa odul bilen kisiler arasinda esit olarak paylastirilacaktir.',
        'the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded' => 'Odul 21Bet hesabiniza eklenecektir ve hesabinizin butun guvenlik asamalarindan gecmis olmasi ve onayli olmasi gerekmektedir.',
        "customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part" => "21Bet promosyonlarından faydalanması daha önce engellenmiş oyuncular bu yarışmaya katılmaları durumunda herhangi bir hak talep edemeyeceklerdir.",
        'players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet' => 'Bu yarışmaya katılmak için 18 yaşından büyük olmalısınız. ',
        'promotions' => 'Promosyonlar',
        'terms_and_conditions' => 'Kural ve Şartlar',
        'betting_rules' => 'Betting Rules',
        'responsible_gambling' => 'Sorumlu Bahis',
        'privacy_policy' => 'Gizlilik Politikası',
        'cookies_policy' => 'Çerezler',
        'contact_us' => 'Bize Ulaşın',
        'copyright' => 'Copyright',
        '2018_21bet_com' => '2018 21bet.com',
        'customers_from_the_UK_are_not_eligible_to_enter' => 'İngiltere’den gelen müşteriler giriş için uygun değil.',
        'review_already_submited' => 'İnceleme Zaten Gönderildi !!',
        'review_submited_successfully' => 'İnceleme Başarıyla Gönderildi !!',
        'success' => 'Başarı!',
   ]

];

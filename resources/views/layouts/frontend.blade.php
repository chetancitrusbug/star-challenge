<!DOCTYPE html>
<html class='no-js' lang='en'>
<head>
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
    <link href='{!! asset('themeb/images/favicon.png') !!}' rel='shortcut icon'>
    <link href='{!! asset('themeb/images/favicon.ico') !!}' rel='icon' type='image/ico'>
    
    @include('frontend.include.cssfiles')
    @yield('headExtra')
    @stack('css')
</head>
<body>
  

<div id='wrapper'>
    @include('frontend.include.topnav')
    @include('frontend.include.banner')
    <div class="content-area clearfix">
    @yield('content')
    </div>
    @include('frontend.include.footer')
</div>

@include('frontend.include.jsfiles')
@include('include.page_notification')
@stack('script-head')
@stack('js')
</body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <title>{{ config('app.name') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta content='text/html;charset=utf-8' http-equiv='content-type'>
        <meta content='Flat administration template for Twitter Bootstrap. ' name='description'>
        <link href="{!! asset('assets/images/favicon.png') !!}" rel='shortcut icon' type='image/png'>
        @include('admin.backend.cssfiles')
        @yield('headExtra')
        @stack('css')
    </head>

    <body>
        <!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			
		</div>
	</div>

        <!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
                                <div class="content">
                                    @yield('content')
                                    @include('admin.backend.footer')
                                </div>
                        </div>
                </div>
        </div>
    </body>
    @include('admin.backend.jsfiles')
	@include('include.page_notification')
    @stack('script-head')
    @stack('js')
</body>
</html>
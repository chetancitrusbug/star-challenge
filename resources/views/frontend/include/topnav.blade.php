<header>
		<div class="header-div clearfix">
			<div class="container">
			<div class="row">
			
				<div class="col-md-4 col-sm-3">
					<div class="logo-div fadeInDown animated"><a href="{{url('')}}"><img src="{!! asset('themeb/images/logo.png') !!}" alt="logo" class="img-responsive" /></a></div><!-- end of logo-div -->
				</div>
				<div class="col-md-8 col-sm-9">
					<div class="top-form-div clearfix">						
						<div class="row">
							<form method="POST" action="{{ url('user-login') }}" id="user-login-form">
								{{ csrf_field() }}
								<div class="col-md-3 col-sm-3 pd5"><input type="text" id="user_email" @if(\Session::has('email')) value="{{\Session::get('email')}}" @endif class="form-control input-1" placeholder="@lang('home.label.email')" name="email"></div>
								<div class="col-md-3 col-sm-3 pd5"><input type="text" id="user_phone" @if(\Session::has('phone')) value="{{\Session::get('phone')}}" @endif class="form-control input-1" placeholder="@lang('home.label.phone')" name="phone"></div>
								<div class="col-md-3 col-sm-3 pd5"><input type="submit" class="btn btn-custom-join setemail" value="@lang('home.label.enter')" ></div>
								<div class="col-md-3 col-sm-3 pd5"><a target="_blank"
										@if(\App::getLocale() && \App::getLocale()=="tr") href="https://www.21bet29.com/home?command=register"
										@elseif(\App::getLocale()) href="https://{{\App::getLocale()}}.21bet.com/home?command=register"
										@else  href="#"
										@endif
										class="btn btn-custom-join">@lang('home.label.join21bet')</a>
								</div>
							</form>  
						</div>					
					</div>
					<div class="lang-dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						  <i class="glyphicon glyphicon-globe"></i> <span class="txt">@lang('home.label.language')</span>
						  <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="langDrop">
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "en"])}}"><img src="{!! asset('themeb/images/lang/en.png') !!}" alt="">English</a></li>
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "no"])}}"><img src="{!! asset('themeb/images/lang/no.png') !!}" alt="">Norway</a></li>
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "fi"])}}"><img src="{!! asset('themeb/images/lang/fi.png') !!}" alt="">Finland</a></li>
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "se"])}}"><img src="{!! asset('themeb/images/lang/se.png') !!}" alt="">Sweden</a></li>
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "tr"])}}"><img src="{!! asset('themeb/images/lang/tr.png') !!}" alt="">Turkey</a></li>
							<li><a href="{{request()->fullUrlWithQuery(['lang'=> "jp"])}}"><img src="{!! asset('themeb/images/lang/jp.png') !!}" alt="">Japan</a></li>
						</ul>
						
					</div>
				</div><!-- end of col -->
			
			</div><!-- end of row -->
			</div><!-- end of container -->
			
		</div><!-- end of header-div -->
	</header>

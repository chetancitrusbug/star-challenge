<link href="https://fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">


<link href="{!! asset('themeb/css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
<link href="{!! asset('themeb/css/custom.css') !!}" media="all" rel="stylesheet" type="text/css" />
<link href="{!! asset('themeb/css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">


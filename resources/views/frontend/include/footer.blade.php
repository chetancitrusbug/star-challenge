<footer>
		<section class="footer-section">
			<h4>@lang('home.label.the_€100k_world_cup_pick_em_terms_and_conditions')</h4>
			<div class="footer-div-tc">
								<ol>
									<li>
										@lang('home.label.qualifying_will_run_from_0001_on_monday_14th_may_and_end_at_2359_on_wednesday_13th_june')
									</li>
									<li>
										@lang('home.label.customers_from_the_UK_are_not_eligible_to_enter')
									</li>
									<li >
										@lang('home.label.to_enter_the_competition_you_must_have_a_21bet_account_before_the_end_of_the_qualifying_period')
									</li>
									<li >
										@lang('home.label.your_email_address_must_match_your_entry_with_your_21bet_account')
									</li>
									<li >
										@lang('home.label.only_1_entry_is_permitted_per_person_per_household')
									</li>
									<li >
										@lang('home.label.to_qualify_for_the100k_cash_prize_you_must_correctly_predict_the_outcome_of_all_48_matches_in_the_group_stages_of_the_world_cup_by_submitting_your_picks_on_the_world_cup_pick_em_page')
									</li>
									<li >
										@lang('home.label.if_more_than_1_person_predicts_correctly_the_prize_pool_will_be_shared_equally')
									</li>
									<li >
										@lang('home.label.the_cash_prize_will_be_paid_into_your_21bet_account_which_must_be_fully_kyc_verified_and_funded')
									</li>
									<li >
										@if(App::getLocale() != 'jp')
											@lang('home.label.customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part')
										@else
											@lang('home.label.customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part_first_line')
											<p>@lang('home.label.customers_who_are_restricted_from_promotions_will_not_be_eligible_to_take_part_second_line')</p>
										@endif
									</li>
									<li >
										@lang('home.label.players_must_be_18_years_of_age_to_enter_or_to_play_on_21bet')
									</li>
								<ol>	
							</div>
							
							<ul class="list-inline">
                                                        
                                                        @if(App::getLocale() == 'tr')
							<li><a href="https://www.21bet29.com/promosyonlar" style="text-transform: capitalize !important;">@lang('home.label.promotions')</a></li>
							<li><a href="https://www.21bet29.com/sartlar-kosullar" style="text-transform: capitalize !important;">@lang('home.label.terms_and_conditions')</a></li>
							<li><a href="https://www.21bet29.com/responsible-gambling" style="text-transform: capitalize !important;">@lang('home.label.betting_rules')</a></li>
							<li><a href="https://www.21bet.com/sartlar-kosullar" style="text-transform: capitalize !important;">@lang('home.label.responsible_gambling')</a></li>
							<li><a href="https://www.21bet29.com/security-privacy" style="text-transform: capitalize !important;">@lang('home.label.privacy_policy')</a></li>
							<li><a href="https://www.21bet29.com/sartlar-kosullar" style="text-transform: capitalize !important;"> @lang('home.label.cookies_policy') </a></li>
							<li><a href="https://www.21bet29.com/iletisim" style="text-transform: capitalize !important;"> @lang('home.label.contact_us') </a></li>
                                                        @else
                                                        <li><a href="https://www.21bet.com/promotions" style="text-transform: capitalize !important;">@lang('home.label.promotions')</a></li>
							<li><a href="https://www.21bet.com/terms-conditions" style="text-transform: capitalize !important;">@lang('home.label.terms_and_conditions')</a></li>
							<li><a href="https://www.21bet.com/responsible-gambling" style="text-transform: capitalize !important;">@lang('home.label.betting_rules')</a></li>
							<li><a href="https://www.21bet.com/responsible-gambling" style="text-transform: capitalize !important;">@lang('home.label.responsible_gambling')</a></li>
							<li><a href="https://www.21bet.com/betting-exchange" style="text-transform: capitalize !important;">@lang('home.label.privacy_policy')</a></li>
							<li><a href="https://www.21bet.com/contact-us" style="text-transform: capitalize !important;"> @lang('home.label.cookies_policy') </a></li>
							<li><a href="https://www.21bet.com/contact-us" style="text-transform: capitalize !important;"> @lang('home.label.contact_us') </a></li>
                                                        @endif
						</ul>
		</section>
		
		<div class="copyright_wrapper"><div class="copyright_text">@lang('home.label.copyright') &copy; @lang('home.label.2018_21bet_com')</div>
	</footer>

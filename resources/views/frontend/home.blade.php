@extends('layouts.frontend')
@section('title',trans('home.label.home'))

@section('content')
         <section class="group_stage_section" id="group_stage">
            <div class="group_stage_div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 wow mob-work-part animation-name-1">
                            <div class="top_group_stage">
                                @if(App::getLocale() != 'jp')
                                    <h4 class="h4-1">@lang('home.label.make_your_picks')</h4> 
                                    <p class="p-1">@lang('home.label.select_the_outcome_of_every_group_stage_match')</p>
                                @else
                                    <p class="p-1">@lang('home.label.select_the_outcome_of_every_group_stage_match')</p>    
                                @endif
                               
                            </div>
                        </div>
                    </div><!-- end of row -->
                </div><!-- end of container -->            
            </div><!-- end of group_stage_div -->
        </section><!-- end of group_stage_section -->

        <section class="group_match_section" id="group_stage">
            <div class="group_match_div">
                <div class="container">
                    @if($matches->count() >0)
                    <form method="POST" action="{{ url('user-review') }}" id="user-review-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_email" value="" id="re_user_email">
                            <input type="hidden" name="user_phone" value="" id="re_user_phone">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="top_group_match">
                                @php( $t_date = "")
                                @foreach($matches as $k=>$match)
                                
                                @php($oldval = $isChecked($match->id) )
                                
                                @if($match->match_date != $t_date && $t_date!="")
                                     </div>
                                </div>
                                @endif
                                @if($match->match_date != $t_date)
                                <div class="col-md-12 col-sm-12 col-xs-12 group-1 margin-top-25">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <p class="p-match"> {{-- {{$match->title}} --}} {{-- {{ \Carbon\Carbon::createFromFormat('Y-m-d',$match->match_date)->format('jS F Y')}} --}}  </p>
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-xs-12">
                                @endif
                                
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12 row-1">
                                            <label class="col-50 text-left" for="teama_{{$match->id}}">
                                                <div class="column matchup-team teama_{{$match->id}} @if($oldval==$match->teama->id) team-selected @endif">
                                                    <img src="{!! asset('uploads/team/'.$match->teama->refimages->image) !!}" height="30" class="matchup-flag">
                                                    <span class="matchup-team-name">{{$match->teama->name}} </span>
                                                </div>
                                            </label>
                                            <label class="center-div " for="draw_{{$match->id}}">
                                                <span class="btn btn-draw draw_{{$match->id}} @if($oldval=='draw') team-selected @endif">
                                                    @lang('home.label.draw')
                                                </span>
                                            </label>
                                            <label class="col-50 text-right " for="teamb_{{$match->id}}">
                                                <div class="column matchup-team teamb_{{$match->id}} @if($oldval==$match->teamb->id) team-selected @endif">
                                                    <span class="matchup-team-name">{{$match->teamb->name}}</span>
                                                    <img src="{!! asset('uploads/team/'.$match->teamb->refimages->image) !!}" height="30"class="matchup-flag">
                                                </div>
                                            </label>
                                            
                                            {!! Form::radio('review['.$match->id.']',$match->teama->id,($oldval==$match->teama->id)? 1 : null ,['class'=>'v_hidden review_radio','id' => 'teama_'.$match->id]) !!}
                                            {!! Form::radio('review['.$match->id.']',"draw",($oldval=="draw")? 1 : null ,['class'=>'v_hidden review_radio','id' => 'draw_'.$match->id]) !!}
                                            {!! Form::radio('review['.$match->id.']',$match->teamb->id,($oldval==$match->teamb->id)? 1 : null ,['class'=>'v_hidden review_radio','id' => 'teamb_'.$match->id]) !!}
                                           
                                            
                                        </div><!-- row-1 -->

                                       
                                        @if($k+1 == $matches->count())
                                        </div>
                                        </div><!-- row-1 -->
                                        @endif
                                   @php( $t_date = $match->match_date)
                                @endforeach

								
								<div class="col-md-12 col-sm-12 col-xs-12 margin-top-25 margin-bottom-25 text-center">
                                
									<div class="col-md-2 col-sm-2 col-xs-12"></div>
									<div class="col-md-10 col-sm-10 col-xs-12">
                                                                            @if(!$review)
                                                                            <button type="submit" class="sbtn">@lang('home.label.submit')</button>
                                                                            @endif
									</div>
                                </div>

                                </div>
                    </form>
                    @endif
                            </div>
                        </div>
                    </div><!-- end of row -->
                </div><!-- end of container -->            
            <!-- end of group_match_div -->
        </section>
	 
@endsection


@push('js')


<script>
$(document).ready(function(){ 
    var option = {"positionClass": "toast-top-right","timeOut": "10000"}; 
    var totalmatch = {{$matches->count()}};
    $(".review_radio").change(function() {
        var pardiv = $(this).parent();
        var s_class = $(this).attr("id");
        
        $(pardiv).find(".team-selected").removeClass("team-selected");
        $(pardiv).find("."+s_class).addClass("team-selected");
        
        if($("#user_email").val()=="" || $("#user_phone").val()==""){
            toastr.warning( "@lang('home.label.action_not_procede')","@lang('home.label.enter_email_phone')",option);
             $('html, body').animate({
                scrollTop: $('#user_email').offset().top
              }, 1000);
             return false;
         }
        
         
    });
    $("#user-review-form").submit(function(){
         if ( $('#user-review-form input:radio:checked').size() != totalmatch )   {
            toastr.warning( "@lang('home.label.action_not_procede')","@lang('home.label.enter_email_phone')",option);
             return false;
         }
         
         if($("#user_email").val()=="" || $("#user_phone").val()==""){
            toastr.warning( "@lang('home.label.action_not_procede')","@lang('home.label.enter_email_phone')",option);
             $('html, body').animate({
                scrollTop: $('#user_email').offset().top
              }, 1000);
             return false;
         }else{
             $("#re_user_email").val($("#user_email").val());
             $("#re_user_phone").val($("#user_phone").val());
         }
         
    });
    $(document).on('click', '.setemail', function (e) {
         if($("#user_email").val()=="" || $("#user_phone").val()==""){
            toastr.warning( "@lang('home.label.action_not_procede')","@lang('home.label.enter_email_phone')",option);
             return false;
         }else{
             return true;
             $('html, body').animate({
                scrollTop: $('#user-review-form').offset().top
             }, 1000);
         }
    });

});
    
</script>
@endpush
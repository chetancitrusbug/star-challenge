@extends('layouts.admin')
@section('title','Teams')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Teams</h5>
        <div class="heading-elements">
             <a href="#" class="btn btn-success btn-sm team_form_open" data-id="0" title="Add New Team">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
             </a>
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
							<tr>
							<th >Image</th>
                            <th >Name</th>
                            <th >Code</th>
                            <th >Group</th>
                            <th >Setting</th>
                            </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include("admin.teams.teamForm")
@endsection
@push('js')
<script>
    
	var pub_url = "{{url('uploads/team')}}";
    
    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            { 
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
					var iamge = "";
					if(o.refimages)
                    {
						iamge = '<img src="'+pub_url+'/'+o.refimages.image+'" alt="team" width="100px" class="viewImage">';
					}
                    return iamge;
                }
            },
			{ "data": "name","name":"name"},
			{ "data": "code","name":"code"},
			{ "data": "group","name":"group"},
            { 
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    var e=""; var d=""; var v="";
					e= "<a href='javascript:void(0);' data-id='"+o.id+"' class='team_form_open' title=''><i class='fa fa-edit action_icon'></i></a>";
                   
					d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='' ><i class='fa fa-trash action_icon '></i></a>";
                   
                    var v =  "";
                    return v+e;
                   
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/teams/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                 
            }
        }
    });
    
    
  
   
</script>
@endpush

<div class="modal modal-static fade" id="team_modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form_model"></h4>
            </div>
            <div class="modal-body">
			
                <div class="panel panel-flat">
                    <div class="panel-body ">
                        {!! Form::open(['url' => '','id'=>'team_form','class' => 'form-horizontal team_form', 'files' => true]) !!}

                        {!! Form::hidden('team_id',0, ['class' => 'form-control','id'=>'team_id']) !!}
                        
						 
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'Name *',['class' => 'control-label']) !!}
                            <input type="text" name="name" class="filter form-control"  id="name" style="">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>

						<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                            {!! Form::label('code', 'Code *',['class' => 'control-label']) !!}
                            <input type="text" name="code" class="filter form-control"  id="code" style="">
                            {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                            
                        </div>
						
                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
                            {!! Form::label('desc', 'Description *',['class' => 'control-label']) !!}
                           <input type="text" name="desc" class="filter form-control"  id="desc" style="">
                                {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
                        </div>

                        

                        <div class="form-group {{ $errors->has('group') ? 'has-error' : ''}}">
                            {!! Form::label('group', 'Group *',['class' => 'control-label']) !!}
                            {!! Form::select('group',['A'=>'A','B'=>'B','C'=>'C','D'=>'D','E'=>'E','F'=>'F','G'=>'G',],null, ['class' => 'full-width','id'=>'group']) !!}
                                {!! $errors->first('group', '<p class="help-block">:message</p>') !!}
                        </div>

                       

                        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                            {!! Form::label('status', 'Status *',['class' => 'control-label']) !!}
                            {!! Form::select('status',['active'=>"Active" ,'inactive'=>'In Active'] ,null, ['class' => 'form-control']) !!}                     
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <div class="form-group newImageArea {{ $errors->has('image') ? 'has-error' : ''}}">
                                {!! Form::label('image', 'Image *', ['class' => 'control-label']) !!}
                                <input type="file" class="form-control" name="image" id="image" >
                                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <div id="imageArea">
                            
                        </div>
                        
						<p class="form_submit_error text-error"></p>

                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-primary btn_form_model']) !!}

                        <button type="button" class="btn btn-warning" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('js')


<script>


/***************action model******************/
        var _model = "#team_modal";
        $(document).on('click', '.team_form_open', function (e) {
            $(".team_form")[0].reset();
            $("#imageArea").html(''); 
            var id=$(this).attr('data-id'); 
		
            $("#team_id").val(id);
            if(id!=0){
                $(".title_form_model").text("Update team");
                var url = "{{url('admin/teams')}}/"+id+"/edit";   
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        var data=result.data;
                         $.each(data, function(key, value){
                            $("#"+key).val(value);
                         });
						 if(data.refimages)
                         {
                            var addString = '<img src="'+pub_url+'/'+data.refimages.image+'" alt="team" width="100px" class="viewImage">';
							$('#imageArea').html(addString);
                         }

                    }
                });
            }else{
                $(".title_form_model").text("Create New team");
            }
            
            $(_model).modal('show');
            return false;
        });


    $('.team_form').submit(function(event) {
        event.preventDefault();
        var error_msg = "";
        if($(".team_form #name").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'name'])<br>";
        }
        if($(".team_form #desc").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'desc'])<br>";
        }
        if($(".team_form #code").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'Code'])<br>";
        }
        if($(".team_form #Group").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'Group'])<br>";
        }
        
		var id = $("#team_id").val();
        if(id == 0)
        {
            if($(".team_form #image").val() != ""){ 
                var file = $('#image').prop('files')[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/svg", "image/gif", "image/jpg", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    error_msg += "* The image must be a file of type: jpeg, png, jpg, gif, svg.<br>";
                }
            }
            else
            {
                error_msg += "@lang('comman.validation.field_required',['field_name'=>'image'])<br>";
            }
        }
        
        if($(".team_form #status").val()==""){
            error_msg += "@lang('comman.validation.field_required',['field_name'=>'status'])<br>";
        }
		
        
        
        if(error_msg!=""){
            $(".form_submit_error").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }
        if($(".team_form #team_id").val()!="" && $(".team_form #team_id").val()!=0){
            var id = $(".team_form #team_id").val();
            var url = "{{url('admin/teams')}}/"+id;
            var method = "POST";
        }
        else
        {
            var url = "{{url('admin/teams')}}";
            var method = "POST";
        }
        $.ajax({
            url: url,
            method: method,
            dataType:'json',
            async:false,
            processData: false,
            contentType: false,
            data:new FormData($("#team_form")[0]),
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {
                $(_model).modal('hide');
                toastr.success('Action Success!', data.message);
                datatable.fnDraw(false);
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
    
</script>
@endpush
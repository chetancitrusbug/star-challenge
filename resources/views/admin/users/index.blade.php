@extends('layouts.admin')
@section('title','Users')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Users</h5>
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
                                <tr>
                                    <th >User Email</th>
                                    <th >User Phone</th>
                                    <th >Total Review</th>
                                    <th >View Review</th>
                                </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include("admin.teams.teamForm")
@endsection
@push('js')
<script>

    var url = "{{url('/admin')}}";
            datatable = $('.datatable').dataTable({
            lengthMenu: [[10, 50, 100, 200, - 1], [10, 50, 100, 200, "All"]],
            pageLength: 10,
            "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
                    "infoEmpty":"@lang('comman.datatable.infoEmpty')",
                    "search": "@lang('comman.datatable.search')",
                    "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
                    "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
                    paginate: {
                    next: '@lang('comman.datatable.paginate.next')',
                            previous: '@lang('comman.datatable.paginate.previous')',
                            first:'@lang('comman.datatable.paginate.first')',
                            last:'@lang('comman.datatable.paginate.last')',
                    }
            },
            responsive: true,
            pagingType: "full_numbers",
            processing: true,
            serverSide: true,
            autoWidth: false,
            stateSave: true,
            order: [0, "desc"],
            columns: [
                { "data": "email","name":"email"},
                { "data": "user_phone","name":"user_phone"},
                { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var cnt = "";
                        if(o.reviews)
                        {
                            cnt = o.reviews.length;
                        }
                        return cnt;
                    }
                },
                { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var lnk = "";
                        if(o.reviews && o.reviews.length > 0)
                        {
                            var lnk = "<a href='"+url+"/review?user_id="+o.id+"' data-id='"+o.id+"' class='' title=''><i class='fa fa-eye action_icon'></i> View</a>";
                        }
                        return lnk;
                    }
                }
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
                    return nRow;
            },
            ajax: {
            url: "{{ url('admin/users/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {

                    }
            }
    });




</script>
@endpush

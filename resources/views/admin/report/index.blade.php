@extends('layouts.admin')
@section('title','Report')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Report</h5>
        <div class="heading-elements">

        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-3 btnbtmspc">
                <div class="col-lg-6">
                    {!! Form::select('match',$match,null, ['class' => 'form-control selectMatchArea']) !!} 
                </div>
                <div class="col-lg-6 downloadcsv" >
                    <button class="btn btn-success btn-sm downloadcsvbtn" title="Download Csv">
                    <i class="fa fa-download" aria-hidden="true"></i> Download Csv </button>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">
                    
                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
                                <tr>
                                    <th >User</th>
                                    <th id="teamaNameId"> Team-A</th>
                                    <th > Draw </th>
                                    <th id="teambNameId"> Team-B</th>
                                </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include("admin.teams.teamForm")
@endsection
@push('js')
<script>
    $('.downloadcsv').hide();
    var user_id = "{!! request()->get('user_id') !!}";
    var pub_url = "{{url('uploads/team')}}";
    var country_url = "{{url('themeb/images/lang')}}";
    var url ="{{ url('admin/report/datatable') }}";
        var datatable = $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            searchable: false,
            orderable: false,
            ajax: {
                    url: '{!! url("admin/report/datatable") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                {
                        "name": "email",
                        "searchable": false,
                        "orderable": false,
                        "data": null,
                        "render": function (o) {
                            var image="";
                            if(o.lang && o.lang !=""){
                                image = '<img src="'+country_url+'/'+o.lang+'.png" alt="">';
                            }
                            return image +"  "+o.email;
                        }
                },
                {
                        "data": null,
                        "name":"teama.name",
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var teama_name = "";
                            $('#teamaNameId').html(o.teama_name);
                             if(o.teama_name == o.teamrev_name){
                                teama_name = "Yes";
                             }else{
                                teama_name = '-';
                             }
                             return teama_name;
                        }
                },
                {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "name":"teama.name",
                        "render": function (o) {
                            var rev = "";
                             if(o.review=="draw"){
                                 rev = "Yes";
                             }else{
                                 rev = '-';
                             }
                             return rev;
                        }
                },
                {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "name":"teamrev.name",
                        "render": function (o) {
                            var teamb_name = "";
                            $('#teambNameId').html(o.teamb_name);
                             if(o.teamb_name == o.teamrev_name){
                                teamb_name = "Yes";
                             }else{
                                teamb_name = '-';
                             }
                             return teamb_name;
                        }
                }
            ]
        });
        
        $('.dataTables_filter').hide();      
    $( ".selectMatchArea" ).change(function() {  
        var match = $( ".selectMatchArea" ).val();
        if(match != '')
        {
            $('.downloadcsv').show();
        }
        else{
            $('.downloadcsv').hide();
        }
        var url ="{!! url('admin/report/datatable') !!}";
        datatable.ajax.url( url + '?match='+ match).load();            
    });

    $( ".downloadcsvbtn" ).click(function() { 
        var match = $( ".selectMatchArea" ).val();
        var url ="{!! url('admin/report/exportcsv') !!}";
        window.location = url + '?match='+ match;
    });
</script>
@endpush

@extends('layouts.admin')
@section('title','Matches')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Create New Match</h5>
        <div class="heading-elements">
            <a href="{{url('/admin/match/create')}}" class="btn btn-success btn-sm team_form_open" data-id="0" title="Add New Match">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
            </a>
        </div>
    </div>



    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
                                <tr>
                                    <th >Title</th>
                                    <th >Date</th>
                                    <th >Team A</th>
                                    <th >Team B</th>
                                    <th >Setting</th>
                                </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>




    var pub_url = "{{url('uploads/team')}}";
            datatable = $('.datatable').dataTable({
            lengthMenu: [[10, 50, 100, 200, - 1], [10, 50, 100, 200, "All"]],
            pageLength: 10,
            "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
                    "infoEmpty":"@lang('comman.datatable.infoEmpty')",
                    "search": "@lang('comman.datatable.search')",
                    "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
                    "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
                    paginate: {
                    next: '@lang('comman.datatable.paginate.next')',
                            previous: '@lang('comman.datatable.paginate.previous')',
                            first:'@lang('comman.datatable.paginate.first')',
                            last:'@lang('comman.datatable.paginate.last')',
                    }
            },
            responsive: true,
            pagingType: "full_numbers",
            processing: true,
            serverSide: true,
            autoWidth: false,
            stateSave: true,
            order: [0, "desc"],
            columns: [

            { "data": "title", "name":"title"},
            { "data": "match_date", "name":"match_date"},
            {
                    "data": null,
                    "name":"teama.name",
                    "render": function (o) {
                        return  o.teama_name + " (" + o.teama_code + ")";
                    }
            },
            {
                    "data": null,
                    "name":"teamb.name",
                    "render": function (o) {
                        return  o.teamb_name + " (" + o.teamb_code + ")";
                    }
            },
            {
            "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                    var e = ""; var d = ""; var v = "";
                            e = "<a href='{{url("admin/match")}}/" + o.id + "/edit' data-id='" + o.id + "' class='team_form_open' title=''><i class='fa fa-edit action_icon'></i></a>";
                            d = "<a href='javascript:void(0);' class='del-item' data-id=" + o.id + " title='' ><i class='fa fa-trash action_icon '></i></a>";
                            var v = "";
                            return v  + e;
                    }
            }

            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
                    return nRow;
            },
            ajax: {
            url: "{{ url('admin/match/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {

                    }
            }
    });




</script>
@endpush

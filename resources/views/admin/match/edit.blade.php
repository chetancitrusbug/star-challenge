@extends('layouts.admin')

@section('title','Update Match')


@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Update Match</h5>
        <div class="heading-elements">
             
        </div>
    </div>

    <div class="panel-body">

        <div class="panel panel-flat">
						

		                <div class="panel-body" style="display: block;">
						@if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
						
						{!! Form::model($match, [
                            'method' => 'PATCH',
                            'url' => ['/admin/match', $match->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'autocomplete'=>'off'
                        ]) !!}
						
						@include ('admin.match.form')
								
	                    {!! Form::close() !!}
					    </div>
					</div>
    </div>
	</div>
        
@endsection



<div class="form-group">
									<label class="col-lg-2 control-label">Team - A v/s Team - B</label>
									<div class="col-lg-10">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group {{ $errors->has('team_a') ? 'has-error' : ''}}">
												{!! Form::select('team_a',$teams, null, ['class'=> 'form-control','id'=>'team_a']) !!}
													{!! $errors->first('team_a', '<p class="help-block">:message</p>') !!}
				                                </div>

				                                
											</div>

											<div class="col-md-6">
												<div class="form-group {{ $errors->has('team_b') ? 'has-error' : ''}}">
												{!! Form::select('team_b',$teams, null, ['class'=> 'form-control','id'=>'team_b']) !!}
													{!! $errors->first('team_b', '<p class="help-block">:message</p>') !!}
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Match Title</label>
									<div class="col-lg-10">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
													{!! Form::text('title',null,['placeholder'=>'Match Title','class' => 'form-control']) !!}
					                                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
				                                </div>
											</div>

										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Match Date</label>
									<div class="col-lg-10">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group {{ $errors->has('match_date') ? 'has-error' : ''}}">
													{!! Form::date('match_date',null,['placeholder'=>'Match Date','class' => 'form-control']) !!}
					                                {!! $errors->first('match_date', '<p class="help-block">:message</p>') !!}
				                                </div>
											</div>

										</div>
									</div>
								</div>

		                        <div class="text-right">
								{!! Form::submit('Submit', ['class' => 'btn 	btn-primary']) !!}
		                        	
		                        </div>
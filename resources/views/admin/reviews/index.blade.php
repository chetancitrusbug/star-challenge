@extends('layouts.admin')
@section('title','Reviews')
@section('content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Reviews by : </h5>
        <div class="heading-elements">
{{$user->email}} | {{$user->user_phone}}
        </div>
    </div>

    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <thead>
                                <tr>
                                    <th >#Id</th>
                                    <th >Match</th>
                                    <th> Team-A V/S Team-B</th>
                                    <th >Review</th>
                                </tr>
                            </thead>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include("admin.teams.teamForm")
@endsection
@push('js')
<script>

    var user_id = "{!! request()->get('user_id') !!}";
    var pub_url = "{{url('uploads/team')}}";
            datatable = $('.datatable').dataTable({
            lengthMenu: [[10, 50, 100, 200, - 1], [10, 50, 100, 200, "All"]],
            pageLength: 10,
            "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
                    "infoEmpty":"@lang('comman.datatable.infoEmpty')",
                    "search": "@lang('comman.datatable.search')",
                    "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
                    "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
                    paginate: {
                    next: '@lang('comman.datatable.paginate.next')',
                            previous: '@lang('comman.datatable.paginate.previous')',
                            first:'@lang('comman.datatable.paginate.first')',
                            last:'@lang('comman.datatable.paginate.last')',
                    }
            },
            responsive: true,
            pagingType: "full_numbers",
            processing: true,
            serverSide: true,
            autoWidth: false,
            stateSave: true,
            order: [0, "asc"],
            columns: [
                { "data": "id","name":"reviews.id"},
                {
                        "name": "match_date",
                        "data": null,
                        "render": function (o) {
                            return o.match_date + " - (" +o.title+")";
                        }
                },
                {
                        "data": null,
                        "name":"teama.name",
                        "render": function (o) {
                            return o.teama_name + " v/s "+o.teamb_name;
                        }
                },
                {
                        "data": null,
                        "searchable": true,
                        "orderable": true,
                        "name":"teamrev.name",
                        "render": function (o) {
                            var rev = "";
                             if(o.review=="draw"){
                                 rev = "Draw";
                             }else{
                                 rev = o.teamrev_name
                             }
                             return rev;
                        }
                }
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
                    return nRow;
            },
            ajax: {
            url: "{{ url('admin/review/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.user_id=user_id;
                    }
            }
    });




</script>
@endpush

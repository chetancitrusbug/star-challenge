@extends('layouts.admin')


@section('title',"Profile")

@section('content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Profile</h5>
        <div class="heading-elements">
            <a href="{{ route('profile.edit') }}" class="btn btn-success btn-sm "  title="Edit Profile">
                <i class="fa fa-plus" aria-hidden="true"></i> Edit Profile
            </a>
             <a href="{{ route('profile.password') }}" class="btn btn-success btn-sm "  title="Change Password">
                <i class="fa fa-plus" aria-hidden="true"></i> Change Password
            </a>
        </div>
    </div>



    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- Marketing campaigns -->
                <div class="panel panel-flat">

                    <div class="table-responsive">
                        <table class="table  datatable-responsive-row-control datatable">

                            <tr>
                                <td class="col-md-2">First Name</td>
                                <td>{{$user->first_name}}</td>
                            </tr>

                            <tr>
                                <td class="col-md-2">Last Name</td>
                                <td>{{$user->last_name}}</td>
                            </tr>

                            <tr>
                                <td class="col-md-2">Name</td>
                                <td>{{$user->name}}</td>
                            </tr>

                            
                            <tr>
                                <td>Email</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


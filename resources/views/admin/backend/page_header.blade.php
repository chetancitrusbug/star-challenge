<section class="top-section">
    <div class="page-header">
        <div class="page-header-content">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 clearfix">
                    <div class="page-title">
                        <h4><span class="text-semibold">Welcome</span> - {{Auth::user()->name}}</h4>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 clearfix">
                    <div class="heading-elements-div">
                        <div class="heading-btn-group-div">
                            <ul class="clearfix ul-dashboard">
                                
                            </ul>	
                        </div>
                    </div>	
                </div>
            </div>
        </div>			
    </div><!-- /page header -->

</section>
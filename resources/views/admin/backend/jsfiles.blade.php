<!-- Core JS files -->
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/loaders/pace.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/core/libraries/jquery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/core/libraries/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/loaders/blockui.min.js') !!}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/visualization/d3/d3.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/visualization/d3/d3_tooltip.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/forms/styling/switch.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/pages/form_checkboxes_radios.js') !!}"></script>

<!-- Temprature range selectin -->
<script type="text/javascript" src="{!! asset('themea/assets/js/core/libraries/jquery_ui/sliders.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/sliders/nouislider.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/sliders/nouislider.js') !!}"></script>



<script type="text/javascript" src="{!! asset('themea/assets/js/core/app.js') !!}"></script>
<!-- /theme JS files -->
<script src="{!! asset('assets/javascripts/toastr.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/moment.min.js')!!}"></script>


<script type="text/javascript" src="{!! asset('themea/assets/js/plugins/forms/selects/select2.min.js') !!}"></script>


{{-- Data Table--}}
<script src="{!! asset('assets/javascripts/datatable/jquery.dataTables.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/datatable/dataTables.responsive.min.js')!!}"></script>

<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>

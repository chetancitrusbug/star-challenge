<!-- Global stylesheets -->

<link href="{!! asset('themea/assets/css/icons/icomoon/styles.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/minified/bootstrap.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/minified/core.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/minified/components.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/minified/colors.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/style.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/custome.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('themea/assets/css/slider/nouislider.css') !!}" rel="stylesheet" type="text/css">


<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
<!-- /global stylesheets -->
<!-- Data Table -->
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
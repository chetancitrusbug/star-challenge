<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->

                    <li class="{{ request()->is('Dashboard') ? 'active' : '' }}" ><a href="{{url('admin')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					{{-- <li class="{{ request()->is('admin/teams') ? 'active' : '' }}" ><a href="{{url('admin/teams')}}"><i class="icon-users"></i> <span>Teams</span></a></li> --}}
					{{-- <li class="{{ request()->is('admin/match') ? 'active' : '' }}" ><a href="{{url('admin/match')}}"><i class="icon-calendar"></i> <span>Match</span></a></li> --}}
					{{-- <li class="{{ request()->is('admin/review') ? 'active' : '' }}" ><a href="{{url('admin/review')}}"><i class="icon-list"></i> <span>Review</span></a></li> --}}

                        <li class="{{ request()->is('admin/users') ? 'active' : '' }}" >
                            <a href="{{url('admin/users')}}">
                                <i class="icon-users"></i> <span>Users</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('admin/report') ? 'active' : '' }}" >
                            <a href="{{url('admin/report')}}">
                                <i class="fa fa-list"></i> <span>Report</span>
                            </a>
                        </li>
                        
                    
                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
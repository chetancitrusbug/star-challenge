<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'matches';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['team_a', 'team_b', 'title', 'match_date', 'created_by'];

    public function teama() {
        return $this->belongsTo('App\Teams', 'team_a', 'id')->with("refimages");
    }

    public function teamb() {
        return $this->belongsTo('App\Teams', 'team_b', 'id')->with("refimages");
    }
    public function reviews()
    {
        return $this->hasMany('App\Reviews', 'match_id', 'id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'desc', 'status', 'created_by', 'updated_by', 'group'];

    public function refimages() {
        return $this->hasOne('App\RefeImages', 'ref_field_id', 'id')->where('refe_table_field_name', 'teams_id');
    }

}

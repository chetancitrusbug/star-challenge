<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['match_id','user_id', 'review','created_by','updated_by'];
	
    

    public function match() {
        return $this->belongsTo('App\Matches', 'match_id', 'id')->with("teama","teamb");
    }
    public function reviewteam() {
        return $this->belongsTo('App\Teams','review', 'id');
    }
    public function reviewer() {
        return $this->belongsTo('App\User','user_id', 'id');
    }
    
}

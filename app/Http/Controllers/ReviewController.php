<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Teams;
use App\Matches;
use App\RefeImages;
use App\Reviews;

use Session;
use Auth;
use Carbon;

class ReviewController extends Controller
{
     public function __construct()
    {
        
    }
    
    public function store(Request $request)
    {
        
        
        $requestData = $request->all();
		
	$rules = array(
            'review' => 'required',
            'user_email' => 'required',
            'user_phone' => 'required',
        );
        
        
        
        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation=$validator->messages()->getMessages();
            $err_arr =array_values($validation);
            return redirect()->back()->withInput()->with('flash_error',$err_arr[0][0]);
        }
        $user = \App\User::userLogin($request->user_email, $request->user_phone);
        foreach ($requestData['review'] as $key => $value) {
            $exist = Reviews::where("user_id",$user->id)->where("match_id",$key)->get();
            
            if($exist->count() > 0){
                return redirect()->back()->with('flash_error', trans('home.label.review_already_submited'));
                break;
            }else{
                $inputData = ["user_id"=>$user->id,"match_id"=>$key,"review"=>$value];
                Reviews::create($inputData);
            }
        }
       
        Session::forget('email');
        Session::forget('phone');
        return redirect()->back()->with('flash_success', trans('home.label.review_submited_successfully'));
    }
    
    
    public function edit($id)
    {
       
        

    }

    public function update($id, Request $request)
    {
        
    }
    

    public function destroy($id,Request $request)
    {
        
    }
    


}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\RefeImages;
use App\Reviews;
use App\Matches;
use DB;
use Yajra\Datatables\Datatables;

class ReportController extends Controller
{
    public function index(Request $request) {
        $match = Matches::select(
            DB::raw("CONCAT(title,' - ',match_date) AS title"),'id')->pluck('title', 'id')->prepend('Select Match ', '');
        return view('admin.report.index' ,compact('match'));
    }

    public function datatable(Request $request) {
        $record = Reviews::select('reviews.*','users.lang','users.country','users.email','users.user_phone','matches.match_date','matches.title','teama.name as teama_name','teamb.name as teamb_name','teamrev.name as teamrev_name');
        $record->join('matches','matches.id','=','reviews.match_id');
        $record->join('teams as teama','matches.team_a','=','teama.id');
        $record->join('teams as teamb','matches.team_b','=','teamb.id');
        $record->join('users','reviews.user_id','=','users.id');
        $record->leftjoin('teams as teamrev','reviews.review','=','teamrev.id');
        if($request->has("match")){
            $record->where('reviews.match_id',$request->match);
        }
        else{
            $record->where('reviews.match_id',0);
        }
        return Datatables::of($record)->make(true);
    }   

    public function exportcsv(Request $request) {
        if($request->has("match")){
            $record = Reviews::select('reviews.*','users.lang','users.country','users.email','users.user_phone','matches.match_date','matches.title','teama.name as teama_name','teamb.name as teamb_name','teamrev.name as teamrev_name')
            ->join('matches','matches.id','=','reviews.match_id')
            ->join('teams as teama','matches.team_a','=','teama.id')
            ->join('teams as teamb','matches.team_b','=','teamb.id')
            ->join('users','reviews.user_id','=','users.id')
            ->leftjoin('teams as teamrev','reviews.review','=','teamrev.id')
            ->where('reviews.match_id',$request->match)
            ->get();
            if(count($record) > 0)
            {   
                $exportArr = array();
                foreach ($record as $key => $value) {
                    $exportArr[$key][] = $value['email'];
                    if($value['teama_name'] == $value['teamrev_name']){
                        $exportArr[$key][] = "Yes";
                    }else{
                        $exportArr[$key][] = '-';
                    }
                    if($value['review'] == 'draw'){
                        $exportArr[$key][] = "Yes";
                    }else{
                        $exportArr[$key][] = '-';
                    }
                    if($value['teamb_name'] == $value['teamrev_name']){
                        $exportArr[$key][] = "Yes";
                    }else{
                        $exportArr[$key][] = '-';
                    }
                }

                $headers = array(
                    "Content-type" => "text/csv",
                    "Content-Disposition" => "attachment; filename=".$record[0]['title'].
                    '-'.$record[0]['match_date'].".csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
            
                $columns = array('User',$record[0]['teama_name'],'Draw',$record[0]['teamb_name']);
            
                $callback = function() use ($exportArr, $columns)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $columns);
            
                    foreach($exportArr as $export) {
                        fputcsv($file, array($export[0],$export[1],$export[2],$export[3]));
                    }
                    fclose($file);
                };
                return \Response::stream($callback, 200, $headers);
            }
            else
            {
                return redirect('admin/report')->with('flash_message', 'Sorry, this match have no review');
            }
        }
        else
        {
            return redirect('admin/report')->with('flash_message', 'Plase select match');
        }
    }

}

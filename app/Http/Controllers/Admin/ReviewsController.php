<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Teams;
use App\RefeImages;
use App\Reviews;

use Session;
use Auth;
use Carbon;

class ReviewsController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $user = null;
        if($request->has("user_id")){
           $user = \App\User::whereId($request->user_id)->first(); 
        }
        if(!$user){
            Session::flash('flash_message',"User review not found !");
            return redirect('admin/users');
        }
        return view('admin.reviews.index',compact("user"));
    }

    public function datatable(Request $request) {
        $record = Reviews::select('reviews.*','users.email','users.user_phone','matches.match_date','matches.title','teama.name as teama_name','teamb.name as teamb_name','teamrev.name as teamrev_name');
        $record->join('matches','matches.id','=','reviews.match_id');
        $record->join('teams as teama','matches.team_a','=','teama.id');
        $record->join('teams as teamb','matches.team_b','=','teamb.id');
        $record->join('users','reviews.user_id','=','users.id');
        $record->leftjoin('teams as teamrev','reviews.review','=','teamrev.id');
        
        if($request->has("user_id")){
            $record->where('reviews.user_id',$request->user_id);
        }
        
        
        return Datatables::of($record)->make(true);
    }

   

    public function destroy($id, Request $request) {
        $item = Teams::where("id", $id)->first();

        $result = array();

        if ($item) {
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');
            ;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');
            ;
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/packages');
        }
    }


}

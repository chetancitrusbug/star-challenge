<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Teams;
use App\Matches;
use App\RefeImages;
use Session;
use Auth;
use Carbon;

class MatchController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        return view('admin.match.index');
    }

    public function datatable(Request $request) {
        $record = Matches::where("matches.id", ">", 0);
        $record->select('matches.*','teama.name as teama_name','teamb.name as teamb_name','teama.code as teama_code','teamb.code as teamb_code');
        $record->join('teams as teama','matches.team_a','=','teama.id');
        $record->join('teams as teamb','matches.team_b','=','teamb.id');
        
        
        return Datatables::of($record)->make(true);
    }

    public function create() {
        $teams = Teams::pluck('name', 'id')->prepend('Select Team', '');
        return view('admin.match.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $result = array();
        $requestData = $request->all();

        $today = \Carbon\Carbon::now()->format("Y-m-d");
                
        $this->validate($request, [
            'team_a' => 'required',
            'team_b' => 'required|different:team_a',
            'title' => 'required',
            'match_date' => 'required|after:'.$today,
        ]);

        $where_filter = "( team_a= '$request->team_a' OR team_b= '$request->team_a' )";
        $exist = Matches::where("match_date", $request->match_date)->whereRaw($where_filter)->first();
        if ($exist) {
            Session::flash('flash_error', "Match already set for team " . $exist->teama->name . " on " . $request->match_date);
            return redirect()->back()->withInput();
        }

        $where_filter = "( team_a= '$request->team_b' OR team_b= '$request->team_b' )";
        $exist = Matches::where("match_date", $request->match_date)->whereRaw($where_filter)->first();
        if ($exist) {
            Session::flash('flash_error', "Match already set for team " . $exist->teamb->name . " on " . $request->match_date);
            return redirect()->back()->withInput();
        }

        // $requestData['match_date'] = \Carbon\Carbon::createFromFormat('m/d/Y',$request->match_date)->format('Y-m-d');

        $module = Matches::create($requestData);
        if ($module) {
            $result['message'] = \Lang::get('comman.responce_msg.item_created_success', ['item' => "Match"]);
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/match');
        }
    }

    public function edit($id) {
        $result = array();
        $match = Matches::with('teama', 'teamb')->where("id", $id)->first();

        $teams = Teams::pluck('name', 'id')->prepend('Select Team', '');
        return view('admin.match.edit', compact('teams', 'match'));
    }

    public function update($id, Request $request) {
        $result = array();
        $requestData = $request->all();
        $today = \Carbon\Carbon::now()->format("Y-m-d");
      
        
        $this->validate($request, [
            'team_a' => 'required',
            'team_b' => 'required|different:team_a',
            'title' => 'required',
            'match_date' => 'required|after:'.$today,
        ]);

        $where_filter = "( team_a= '$request->team_a' OR team_b= '$request->team_a' )";
        $exist = Matches::where("id", "!=", $id)->where("match_date", $request->match_date)->whereRaw($where_filter)->first();
        if ($exist) {
            Session::flash('flash_error', "Match already set for team " . $exist->teama->name . " on " . $request->match_date);
            return redirect()->back()->withInput();
        }

        $where_filter = "( team_a= '$request->team_b' OR team_b= '$request->team_b' )";
        $exist = Matches::where("id", "!=", $id)->where("match_date", $request->match_date)->whereRaw($where_filter)->first();
        if ($exist) {
            Session::flash('flash_error', "Match already set for team " . $exist->teamb->name . " on " . $request->match_date);
            return redirect()->back()->withInput();
        }

        $item = Matches::where("id", $id)->first();

        if ($item) {
            $item->update($requestData);
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');
            ;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            ;
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/match');
        }
    }

    public function destroy($id, Request $request) {
        $item = Matches::where("id", $id)->first();

        $result = array();

        if ($item) {
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');
            ;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');
            ;
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/packages');
        }
    }

}

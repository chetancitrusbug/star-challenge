<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Teams;
use App\RefeImages;
use Session;
use Auth;
use Carbon;

class TeamController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        return view('admin.teams.index');
    }

    public function datatable(Request $request) {
        $record = Teams::with('refimages')->where("id", ">", 0);
        return Datatables::of($record)->make(true);
    }

    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $result = array();
        $requestData = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'code' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required',
        ]);



        $module = Teams::create($requestData);
        if ($module) {
            $name = $this->uploadPhoto($request, $module->id);
            $result['message'] = \Lang::get('comman.responce_msg.item_created_success', ['item' => "Team"]);
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);
    }

    public function edit($id) {
        $result = array();
        $item = Teams::with('refimages')->where("id", $id)->first();

        if ($item) {
            $result['data'] = $item;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);
    }

    public function update($id, Request $request) {
        $result = array();
        $requestData = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'code' => 'required',
            'status' => 'required',
        ]);

        $item = Teams::where("id", $id)->first();

        if ($item) {
            $item->update($requestData);
            $name = $this->uploadPhoto($request, $item->id);
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');
            ;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            ;
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/packages');
        }
    }

    public function destroy($id, Request $request) {
        $item = Teams::where("id", $id)->first();

        $result = array();

        if ($item) {
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');
            ;
            $result['code'] = 200;
        } else {
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');
            ;
            $result['code'] = 400;
        }

        if ($request->ajax()) {
            return response()->json($result, $result['code']);
        } else {
            Session::flash('flash_message', $result['message']);
            return redirect('admin/packages');
        }
    }

    public function uploadPhoto(Request $request, $id) {
        if ($request->file('image')) {
            $fimage = $request->file('image');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('uploads/team/'), $filename);


            $this->removeImageById($id);

            $requestData = array();
            $requestData['image'] = $filename;
            $requestData['ref_field_id'] = $id;
            $requestData['refe_table_field_name'] = 'teams_id';
            RefeImages::create($requestData);
        }
    }

    public function removeImageById($id) {
        $re = RefeImages::where("refe_table_field_name", "teams_id")->where("ref_field_id", $id)->first();
        if ($re) {
            $image_path1 = public_path() . "/uploads/team/" . $re->image;
            if ($re->image && $re->image != "" && \File::exists($image_path1)) {
                unlink($image_path1);
            }
            $re->delete();
        }
    }

    public function search(Request $request) {
        $result = array();

        $data = Teams::select('id', 'name', 'code');

        if ($request->has('filter_team') && $request->get('filter_team') != '') {
            $data->where('name', 'LIKE', "%$request->filter_team%");
        }
        if ($request->has('selected_team_a') && $request->get('selected_team_a') != '') {
            $data->where('id', '!=', $request->selected_team_a);
        }


        $result['data'] = $data->get();

        return response()->json($result, 200);
    }

}

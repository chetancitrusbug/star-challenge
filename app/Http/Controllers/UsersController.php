<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\User;

use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    
    public function userLogin(Request $request)
    {
        $result = array();
        $error = "";
        $rules = array(
            'email' => 'required|email',
            'phone' => 'required'
        );
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation=$validator->messages()->getMessages();
            $err_arr =array_values($validation);
            if(isset($err_arr) && isset($err_arr[0])){
                return redirect()->back()->withInput()->with('flash_error',$err_arr[0][0]);
            }
        }
        Session::put('email',$request->email);
        Session::put('phone',$request->phone);

        Session::flash('flash_success',trans('home.label.success'));
        return redirect()->back();
    }

   /* public function userLogin(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation=$validator->messages()->getMessages();
            $err_arr =array_values($validation);
            if(isset($err_arr) && isset($err_arr[0])){
                return redirect()->back()->withInput()->with('flash_error',$err_arr[0][0]);
            }
        }
        $requestData = $request->all();
        $user = User::where("email", $request->email)->first();
        
        if(!$user){
            $requestData['password'] =  bcrypt($request->password);
            $user = User::create($requestData);
        }
        
        if($user){
             if (Hash::check($request->password, $user->password)) {
                 Auth::login($user, true);
                 Session::flash('flash_success',"Login Success");
             }else{
                 Session::flash('flash_error',"Invalid username or password");
             }
        }else{
            Session::flash('flash_error',\Lang::get('comman.responce_msg.something_went_wrong'));
        }

        return redirect()->back();
       // return redirect('admin/users')->with('flash_message', 'User added!');
    }*/

    

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Matches;
use App\Reviews;
use App\User;


use Session;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $q = "ALTER TABLE `users` ADD `user_phone` VARCHAR(191) NULL AFTER `email`";
       //  \DB::statement($q);
         
       /*  $q = "DELETE FROM `migrations` WHERE `migration` = '2018_05_09_092350_create_reviews_table'";
         \DB::statement($q);
         
         $q = "DROP TABLE reviews";
         \DB::statement($q);
         
         $q = "DELETE FROM `migrations` WHERE `migration` = '2014_10_12_000000_create_users_table'";
         \DB::statement($q);
         
         $q = "DROP TABLE users";
         \DB::statement($q);
         exit;*/
         
        $matches = Matches::with('teama','teamb')->orderBy('match_date','asc')->get();
        $review = null ;
        
        $user_review = [];
        if(Session::has('email')){
            $user = User::where("email", Session::get('email'))->first();
            if($user){
                $user_review = Reviews::where("user_id",$user->id)->pluck("review","match_id");
                $review = Reviews::where("user_id",$user->id)->get();
            }
        }else if(Auth::user()){
            $user_review = Reviews::where("user_id",Auth::user()->id)->pluck("review","match_id");
        }
        
        
        $isChecked = function ($matchid) use ($user_review){
            if(isset($user_review[$matchid])){
                return $user_review[$matchid];
            }else{
                return null;
            }
        };
        
        return view('frontend.home',compact('matches','isChecked','review'));
    }
}

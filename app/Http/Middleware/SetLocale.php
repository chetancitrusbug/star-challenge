<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //echo Session::get('lang');
        
        if (isset($_REQUEST['lang']) && $_REQUEST['lang'] != null) {
            $Sessionlocale = $_REQUEST['lang'];
            if ($Sessionlocale == 'en' || $Sessionlocale == 'fi' || $Sessionlocale == 'jp' || $Sessionlocale == 'no' || $Sessionlocale == 'se' || $Sessionlocale == 'tr') {
                $locale = $Sessionlocale;
                Session::put('lang',$locale);
            }
            else
            {
                $locale = 'en';
            }
        } 
        else if (Session::has('lang')) {
            $locale = Session::get('lang');
        } 
        else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            if ($locale != 'en' && $locale != 'fi' && $locale != 'jp' && $locale != 'no' && $locale != 'se' && $locale != 'tr') {
                $locale = 'en';
            }
        }
        App::setLocale($locale);
        return $next($request);
    }
}

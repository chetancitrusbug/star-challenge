<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type','first_name','last_name','user_phone','lang','country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static function userLogin($user_email, $user_phone)
    {
        $languages = config('language.lang');
        $clang = \App::getLocale();
        $user = User::where("email", $user_email)->first();
        
        if(!$user){
            $requestData = [];
            $requestData['password'] =  bcrypt($user_phone);
            $requestData['email'] =  $user_email;
            $requestData['user_phone'] =  $user_phone;
            $requestData['type'] =  "user";
            $requestData['lang'] = $clang;
            if(isset($languages[$clang])){
                 $requestData['country'] = $languages[$clang];
            }
            $user = User::create($requestData);
        }else{
            $user->lang = \App::getLocale();
            if(isset($languages[$clang])){
                 $user->country = $languages[$clang];
            }
            $user->save();
        }
        return $user;
       // return redirect('admin/users')->with('flash_message', 'User added!');
    }
    public function reviews()
    {
        return $this->hasMany('App\Reviews', 'user_id', 'id');
    }
}

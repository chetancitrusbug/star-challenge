<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RefeImages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'refe_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'refe_table_field_name', 'ref_field_id', 'image', 'image_ref_code'];


   


}

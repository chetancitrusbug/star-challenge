<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');
Route::post('user-login', 'UsersController@userLogin');

Route::resource('user-review', 'ReviewController');


Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function () {
	
	Route::get('/', 'Admin\AdminController@index');
	
	//Route::get('teams/datatable', 'Admin\TeamController@datatable');
	//Route::post('teams/{id}', 'Admin\TeamController@update');
	//Route::get('teams/search', 'Admin\TeamController@search');
	//Route::resource('teams', 'Admin\TeamController');
	
	//Route::get('match/datatable', 'Admin\MatchController@datatable');
	//Route::resource('match', 'Admin\MatchController');
        
        Route::get('review/datatable', 'Admin\ReviewsController@datatable');
        Route::resource('review', 'Admin\ReviewsController');
	
	Route::resource('roles', 'Admin\RolesController');
	Route::resource('permissions', 'Admin\PermissionsController');
	
        Route::get('users/datatable', 'Admin\UsersController@datatable');
		Route::resource('users', 'Admin\UsersController');
		
		// report

		Route::get('report', 'Admin\ReportController@index');
		Route::get('report/datatable', 'Admin\ReportController@datatable');
		Route::get('report/exportcsv', 'Admin\ReportController@exportcsv');
		
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        //
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
	
	Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

});